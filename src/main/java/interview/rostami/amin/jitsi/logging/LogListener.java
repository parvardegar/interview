package interview.rostami.amin.jitsi.logging;

import interview.rostami.amin.jitsi.logging.model.Log;
import interview.rostami.amin.jitsi.logging.parser.ParserProvider;
import interview.rostami.amin.jitsi.logging.processing.ProxyProcessor;
import org.apache.commons.io.input.Tailer;
import org.apache.commons.io.input.TailerListenerAdapter;

public class LogListener extends TailerListenerAdapter {

    private ProxyProcessor processor;
    private static Log.Builder currentLog = null;

    //TODO: use DI
    public LogListener() {
        processor = new ProxyProcessor();
    }

    @Override
    public void init(Tailer tailer) {
        currentLog = null;
    }

    @Override
    public void handle(String line) {
        aggregateLogLinesAndProcessFullLogs(line);
    }

    @Override
    public void handle(Exception ex) {
        ex.printStackTrace(); //TODO: use a logger
    }

    /*
     * StringBuilder is not thread safe and StringBuffer has some problems with how Tailer works
     * so we needs to make this method synchronized
     */
    synchronized private void aggregateLogLinesAndProcessFullLogs(String line) {

        if (ParserProvider.parser().isStartOfNewLog(line)) {

            if (currentLog != null) {
                processLog(currentLog.build()); //finish reading all log lines and start to process
                currentLog = null;
            }

            currentLog = Log.builder(line);

        } else {

            if (currentLog != null)
                currentLog.line(line);

        }

    }

    private void processLog(Log log) {
        try {
            processor.process(log);
        } catch (Exception e) {
            //TODO use a logger
            e.printStackTrace();
            System.out.println("process exception for log type " + log.getType().name() + ": " + e.getMessage());
        }
    }

}
