package interview.rostami.amin.jitsi.logging.logger;

import interview.rostami.amin.Configs;
import interview.rostami.amin.jitsi.logging.model.Log;
import interview.rostami.amin.jitsi.logging.model.LogType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

//TODO: use FileChannel if the file is large
public class FileLogger implements Logger {

    //TODO: make it better
    // to prevent multiple time append of nicks to file
    //user jid -> nick
    Map<String, String> nicks = new HashMap<>();

    @Override
    public void append(Log log) {
        if (LogType.new_nick.equals(log.getType())) {
            String jid = log.getFromJid();
            String nick = log.getProperty("nick");
            if (!(nicks.containsKey(jid) && nicks.get(jid).equals(nick))) {
                nicks.put(jid, nick);
                appendToFile(log);
            }
        } else if (hasValidType(log)) {
            appendToFile(log);
        }
    }

    //TODO: check performance
    private void appendToFile(Log log) {
        try {

            Files.write(getLogFilePath(), log.toString().getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);

        } catch (IOException e) {
            //TODO: use logger
            e.printStackTrace();
        }
    }

    private Path getLogFilePath() {
        return Paths.get(Configs.getFileLoggerOutputDir(), Configs.getFileLoggerLogFileName());
    }

}
