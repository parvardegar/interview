package interview.rostami.amin.jitsi.logging.parser;

import interview.rostami.amin.jitsi.logging.model.Log;
import interview.rostami.amin.jitsi.logging.model.LogType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//extracts data from log.message
//TODO: we can have some LogTypeParser.parse class that return type and properties for the log instead of parse all types here
public class EjabberdLogMessageParser implements LogParser {

    //each new log starts with timestamp
    @Override
    public boolean isStartOfNewLog(String line) {
        final Pattern pattern = Pattern
                .compile("^\\d\\d\\d\\d-\\d\\d-\\d\\d\\s\\d\\d:\\d\\d:\\d\\d.*", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(line);
        return matcher.matches();
    }

    @Override
    public String parseTimestamp(Log log) {
        int timestampLength = "2021-10-10 16:58:42"/*example timestamp from ejabberd logs*/.toCharArray().length;
        return log.getMessage().substring(0/*from start*/, timestampLength);
    }

    @Override
    public String parseFromUserJid(Log log) {
        String message = log.getMessage();

        int startOfFromSection = message.indexOf("from =");
        int endOfFromSection = message.indexOf("to =");

        return parseUserJid(message, startOfFromSection, endOfFromSection);
    }

    @Override
    public String parseToUserJid(Log log) {
        String message = log.getMessage();

        int startOfToSection = message.indexOf("to =");
        int endOfToSection = message.indexOf("show =");

        return parseUserJid(message, startOfToSection, endOfToSection);
    }

    private String parseUserJid(String message, int startOfFromSection, int endOfFromSection) {
        String userSection = message.substring(startOfFromSection, endOfFromSection);

        String startUserJidSection = "user = <<\"";
        String endUserJidSection = "\">>";

        int startOfUserJidSection = userSection.indexOf(startUserJidSection) + startUserJidSection.length();
        int endOfUserJidSection = userSection.indexOf(endUserJidSection);

        return userSection.substring(startOfUserJidSection, endOfUserJidSection);
    }

    @Override
    public String parseNewNick(Log log) {
        String message = log.getMessage();

        int startOfNickSection = message.indexOf("name = <<\"nick\">>,");
        String nickSection = message.substring(startOfNickSection);
        String nickNameSection = nickSection.split("\n")[2];

        return nickNameSection.substring(nickNameSection.indexOf("<<") + 2, nickNameSection.indexOf(">>"));
    }

    @Override
    public LogType parseType(Log log) {
        //check for new_nick log type
        if (log.getMessage().contains("Routing to MUC room") && log.getMessage().contains("http://jabber.org/protocol/nick")) {
            return LogType.new_nick;
        }

        return LogType.unknown;
    }

}
