package interview.rostami.amin.jitsi.logging.processing;

import interview.rostami.amin.jitsi.logging.model.Log;

public interface LogProcessor {

    void process(Log log);

}
