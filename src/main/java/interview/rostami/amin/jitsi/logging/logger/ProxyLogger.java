package interview.rostami.amin.jitsi.logging.logger;

import interview.rostami.amin.jitsi.logging.model.Log;

import java.util.HashSet;
import java.util.Set;

public class ProxyLogger implements Logger {

    private final Set<Logger> loggers;

    //TODO: use DI
    public ProxyLogger() {
        loggers = new HashSet<>();
        loggers.add(new ConsoleLogger());
        loggers.add(new FileLogger());
    }


    @Override
    public void append(Log log) {
        loggers
                .forEach(logger -> logger.append(log));
    }

}
