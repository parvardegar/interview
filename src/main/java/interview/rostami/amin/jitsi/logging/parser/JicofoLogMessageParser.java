package interview.rostami.amin.jitsi.logging.parser;

import interview.rostami.amin.jitsi.logging.model.Log;
import interview.rostami.amin.jitsi.logging.model.LogType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//extracts data from log.message
//TODO: we can have some LogTypeParser.parse class that return type and properties for the log instead of parse all types here
public class JicofoLogMessageParser implements LogParser {

    //each new log starts with timestamp
    @Override
    public boolean isStartOfNewLog(String line) {
        final Pattern pattern = Pattern
                .compile("^[a-zA-Z][a-z][a-z]\\s\\d\\d,\\s\\d\\d\\d\\d.*", Pattern.CASE_INSENSITIVE);
        final Matcher matcher = pattern.matcher(line);
        return matcher.matches();
    }

    @Override
    public String parseTimestamp(Log log) {
        int timestampLength = "Oct 12, 2021 4:38:16 AM "/*example timestamp from ejabberd logs*/.toCharArray().length;
        return log.getMessage().substring(0/*from start*/, timestampLength);
    }

//    INFO: Receive session-accept from aa@muc.meet.jitsi/71386276
//    INFO: Terminate session: aa@muc.meet.jitsi/71386276, reason: gone, send terminate: false
    @Override
    public String parseFromUserJid(Log log) {
        String message = log.getMessage();

        if (LogType.join_room.equals(log.getType())) {
            String joinMessage = message.substring(message.indexOf("Receive session-accept from "));
            String jid = joinMessage.split("/")[1];
            return jid;
        }

        if (LogType.leave_room.equals(log.getType())) {
            String leaveMessage = message.substring(message.indexOf("Terminate session: "));
            String jid = leaveMessage.split("/")[1].split(",")[0];
            return jid;
        }

        return null;
    }

    @Override
    public String parseToUserJid(Log log) {
        String message = log.getMessage();

        if (LogType.join_room.equals(log.getType())) {
            String joinMessage = message.substring(message.indexOf("Receive session-accept from "));
            String jid = joinMessage.replace("Receive session-accept from ", "").split("@")[0];
            return jid;
        }

        if (LogType.leave_room.equals(log.getType())) {
            String leaveMessage = message.substring(message.indexOf("Terminate session: "));
            String jid = leaveMessage.replace("Terminate session: ", "").split("@")[0];
            return jid;
        }

        return null;
    }

    @Override
    public String parseNewNick(Log log) {
        String message = log.getMessage();

        int startOfNickSection = message.indexOf("name = <<\"nick\">>,");
        String nickSection = message.substring(startOfNickSection);
        String nickNameSection = nickSection.split("\n")[2];

        return nickNameSection.substring(nickNameSection.indexOf("<<") + 2, nickNameSection.indexOf(">>"));
    }

    @Override
    public LogType parseType(Log log) {
        if (log.getMessage().contains("Receive session-accept from")) {
            return LogType.join_room;
        }

        if (log.getMessage().contains("Terminate session:") && log.getMessage().contains("reason: gone")) {
            return LogType.leave_room;
        }

        return LogType.unknown;
    }

}
