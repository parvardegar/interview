package interview.rostami.amin.jitsi.logging.model;

public enum LogType {
    join_room, leave_room, new_nick, unknown, not_processed
}