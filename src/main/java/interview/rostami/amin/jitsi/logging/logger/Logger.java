package interview.rostami.amin.jitsi.logging.logger;

import interview.rostami.amin.jitsi.logging.model.Log;
import interview.rostami.amin.jitsi.logging.model.LogType;


//TODO: we can make different modules to handle log appending and log append formatting and ...
public interface Logger {

    void append(Log log);

    default boolean hasValidType(Log log) {
        return ! (LogType.not_processed.equals(log.getType()) || LogType.unknown.equals(log.getType()));
    }

}
