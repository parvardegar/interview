package interview.rostami.amin.jitsi.logging.processing;

import interview.rostami.amin.jitsi.logging.logger.Logger;
import interview.rostami.amin.jitsi.logging.logger.ProxyLogger;
import interview.rostami.amin.jitsi.logging.model.Log;

public class LoggerInvokerProcessor implements LogProcessor {

    private final Logger proxyLogger = new ProxyLogger();

    @Override
    public void process(Log log) {
        proxyLogger.append(log);
    }

}
