package interview.rostami.amin.jitsi.logging.processing;

import interview.rostami.amin.jitsi.logging.model.Log;
import interview.rostami.amin.jitsi.logging.model.LogType;
import interview.rostami.amin.jitsi.logging.parser.ParserProvider;


public class LogMessageParserProcessor implements LogProcessor {

    @Override
    public void process(Log log) {
        parseAndEnrich(log);
    }

    private void parseAndEnrich(Log log) {
        parseTimetampAndSet(log);
        parseTypeAndSet(log);
        parsePropertiesAndSet(log);
    }

    private void parseTimetampAndSet(Log log) {
        log.setTimestmap(ParserProvider.parser().parseTimestamp(log));
    }

    private void parseTypeAndSet(Log log) {
        log.setType(ParserProvider.parser().parseType(log));
    }

    private void parsePropertiesAndSet(Log log) {
        if (LogType.unknown.equals(log.getType())) return;

        log.setFromJid(ParserProvider.parser().parseFromUserJid(log));
        log.setToJid(ParserProvider.parser().parseToUserJid(log));

        if (LogType.new_nick.equals(log.getType())) {
            log.addProperty("nick", ParserProvider.parser().parseNewNick(log));
        }
    }

}
