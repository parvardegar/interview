package interview.rostami.amin.jitsi.logging.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Ejabberd Users activity Log model
 */
public class Log {

    private String timestmap;
    private String message;
    private LogType type;
    private String fromJid;
    private String toJid; //room
    private Map<String, String> properties;

    private Log(String message) {
        this.message = message;
        this.type = LogType.not_processed;
        this.properties = new HashMap<>();
    }

    public String getMessage() {
        return message;
    }

    public void addProperty(String name, String value) {
        properties.put(name, value);
    }

    public LogType getType() {
        return type;
    }

    public void setType(LogType type) {
        this.type = type;
    }

    public void setFromJid(String fromJid) {
        this.fromJid = fromJid;
    }

    public void setToJid(String toJid) {
        this.toJid = toJid;
    }

    public void setTimestmap(String timestmap) {
        this.timestmap = timestmap;
    }

    @Override
    public String toString() {
        return "timestmap='" + timestmap + '\'' +
                ", action=" + type +
                ", room='" + toJid + '\'' +
                ", jid='" + fromJid + '\'' +
                ", properties=" + properties + "\n";
    }

    public static class Builder {
        private StringBuilder message;

        private Builder(StringBuilder message) {
            this.message = message;
        }

        public Builder line(String line) {
            message.append(line).append('\n');
            return this;
        }

        public Log build() {
            return new Log(message.toString());
        }

    }

    public String getFromJid() {
        return fromJid;
    }

    public String getToJid() {
        return toJid;
    }

    public String getProperty(String name) {
        return this.properties.get(name);
    }

    public static Builder builder(String message) {
        return new Builder(new StringBuilder(message));
    }

}