package interview.rostami.amin.jitsi.logging.parser;

import interview.rostami.amin.jitsi.logging.model.Log;
import interview.rostami.amin.jitsi.logging.model.LogType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//extracts data from log.message
//TODO: we can have some LogTypeParser.parse class that return type and properties for the log instead of parse all types here
public class JicofoXMLLogMessageParser implements LogParser {

    //each new log starts with timestamp
    @Override
    public boolean isStartOfNewLog(String line) {
        return line.startsWith("<record>");
    }

    @Override
    public String parseTimestamp(Log log) {
        String startTag = "<date>";
        String endTag = "</date>";

        String message = log.getMessage();
        int start = message.indexOf(startTag) + startTag.length();
        int end = message.indexOf(endTag);

        return log.getMessage().substring(start, end);
    }

    // org.jitsi.impl.protocol.xmpp.ChatRoomImpl for join_room, leave_room
    // org.jitsi.impl.protocol.xmpp.log.PacketDebugger for new_nick
    @Override
    public LogType parseType(Log log) {
        if (log.getMessage().contains("ChatRoomImpl") && log.getMessage().contains("Joined") && log.getMessage().contains("room:")) {
            return LogType.join_room;
        }

        if (log.getMessage().contains("ChatRoomImpl") && log.getMessage().contains("Left") && log.getMessage().contains("room:")) {
            return LogType.leave_room;
        }

        if (log.getMessage().contains("PacketDebugger") && log.getMessage().contains("http://jabber.org/protocol/nick")) {
            return LogType.new_nick;
        }

        return LogType.unknown;
    }

    @Override
    public String parseFromUserJid(Log log) { //from
        String message = getMessageTag(log);

        if (LogType.join_room.equals(log.getType()) || LogType.leave_room.equals(log.getType())) {
            String jid = message.split(" ")[1].split("/")[1];
            return jid;
        }

        if (LogType.new_nick.equals(log.getType())) {
            String jid = message.split(" ")[5].replace("from=", "")
                    .replace("'", "").split("/")[1];
            return jid;
        }

        return null;
    }

    @Override
    public String parseToUserJid(Log log) { //to
        String message = getMessageTag(log);

        if (LogType.join_room.equals(log.getType()) || LogType.leave_room.equals(log.getType())) {
            String jid = message.split(" ")[1].split("@")[0];
            return jid;
        }

        if (LogType.new_nick.equals(log.getType())) {
            String jid = message.split(" ")[5].replace("from=", "")
                    .replace("'", "").split("@")[0];
            return jid;
        }

        return null;
    }

    private String getMessageTag(Log log) {
        String startTag = "<message>";
        String endTag = "</message>";

        String rawMessage = log.getMessage();
        int start = rawMessage.indexOf(startTag) + startTag.length();
        int end = rawMessage.indexOf(endTag);

        return rawMessage.substring(start, end);
    }

    @Override
    public String parseNewNick(Log log) {
        String message = getMessageTag(log);

        String startToken = "http://jabber.org/protocol/nick\"";
        int start = message.lastIndexOf(startToken) + startToken.length();
        String nick = message.substring(start);
        String name = nick.substring(0, nick.indexOf("/nick"))
                .replace("&gt;", "")
                .replace("&lt;", "");

        return name;
    }

}
