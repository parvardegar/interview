package interview.rostami.amin.jitsi.logging.parser;

import interview.rostami.amin.jitsi.logging.model.Log;
import interview.rostami.amin.jitsi.logging.model.LogType;

public interface LogParser {

    boolean isStartOfNewLog(String line);

    String parseTimestamp(Log log);

    String parseFromUserJid(Log log);

    String parseToUserJid(Log log);

    String parseNewNick(Log log);

    LogType parseType(Log log);

}
