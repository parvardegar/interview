package interview.rostami.amin.jitsi.logging.parser;

import interview.rostami.amin.Configs;

public class ParserProvider {

    private final LogParser parser;
    private static ParserProvider instance;

    //TODO: use DI
    private ParserProvider() {
        switch (Configs.getLogFormat()) {
            case JICOFO: this.parser = new JicofoLogMessageParser();
                System.out.println("use jicofo parser."); break;
            case EJABBERD: this.parser = new EjabberdLogMessageParser();
                System.out.println("use ejabberd parser."); break;
            case JICOFO_XML: this.parser = new JicofoXMLLogMessageParser();
                System.out.println("use jicofoXML parser."); break;
            default: throw new RuntimeException("ERROR: can't initialize Parser, unknown parser name");
        }
    }

    private static ParserProvider getInstance() {
        if (instance == null)
            instance = new ParserProvider();

        return instance;
    }

    public static LogParser parser() {
        return ParserProvider.getInstance().parser;
    }

}
