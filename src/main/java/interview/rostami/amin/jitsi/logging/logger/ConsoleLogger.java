package interview.rostami.amin.jitsi.logging.logger;

import interview.rostami.amin.jitsi.logging.model.Log;

public class ConsoleLogger implements Logger {

    @Override
    public void append(Log log) {
        if(hasValidType(log))
            System.out.println("console-logger: " + log);
    }

}
