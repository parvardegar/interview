package interview.rostami.amin.jitsi.logging.processing;

import interview.rostami.amin.jitsi.logging.model.Log;

import java.util.ArrayList;
import java.util.List;

public class ProxyProcessor implements LogProcessor {

    private final List<LogProcessor> sortedProxiedProcessors;

    //TODO: use DI
    public ProxyProcessor() {
        sortedProxiedProcessors = new ArrayList<>();
        // sorting is important
        sortedProxiedProcessors.add(new LogMessageParserProcessor());
        sortedProxiedProcessors.add(new LoggerInvokerProcessor());
    }


    @Override
    public void process(Log log) {
        sortedProxiedProcessors
                .forEach(processor -> processor.process(log));
    }

}
