package interview.rostami.amin;

public class Configs {

    private final static String DEFAULT_SOURCE_LOG_FILE = "/home/amin/Documents/qarar/stream.log";
    private final static String DEFAULT_FILE_LOGGER_OUTPUT_DIR = "/home/amin/Documents/qarar/";
    private final static String DEFAULT_FILE_LOGGER_LOG_FILE_NAME = "activity.log";

    private final static long DEFAULT_TAILER_DELAY_MILLIS = 0;
    private final static boolean DEFAULT_TAILER_START_FROM_END = false;

    private final static String DEFAULT_LOG_FORMAT = LogFormat.JICOFO_XML.name();
    public enum LogFormat {
        EJABBERD, JICOFO, JICOFO_XML
    }

    public static String getLogFileFullPath() {
        String fromEnv = System.getenv("SOURCE_LOG_FILE");
        return fromEnv == null? DEFAULT_SOURCE_LOG_FILE : fromEnv;
    }

    public static String getFileLoggerOutputDir() {
        String fromEnv = System.getenv("FILE_LOGGER_OUTPUT_DIR");
        return fromEnv == null? DEFAULT_FILE_LOGGER_OUTPUT_DIR: fromEnv;
    }

    public static String getFileLoggerLogFileName() {
        String fromEnv = System.getenv("FILE_LOGGER_LOG_FILE_NAME");
        return fromEnv == null? DEFAULT_FILE_LOGGER_LOG_FILE_NAME: fromEnv;
    }

    public static long getTailerDelayMillis() {
        String fromEnv = System.getenv("TAILER_DELAY_MILLIS");
        return fromEnv == null? DEFAULT_TAILER_DELAY_MILLIS: Long.parseLong(fromEnv);
    }

    public static boolean getTailerStartFromEnd() {
        String fromEnv = System.getenv("TAILER_START_FROM_END");
        return fromEnv == null? DEFAULT_TAILER_START_FROM_END: Boolean.parseBoolean(fromEnv);
    }

    public static LogFormat getLogFormat() {
        String fromEnv = System.getenv("LOG_FORMAT");
        return fromEnv == null? LogFormat.valueOf(DEFAULT_LOG_FORMAT): LogFormat.valueOf(fromEnv);
    }

}
